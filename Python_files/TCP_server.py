import socket
import threading
from threading import Thread
import select
import time
import cv2
import numpy as np


# ==================== Wątek serwera ====================
class ConnectionListenerThread(Thread):
    # Wątek ten jest odpowiedzialny za obsługę serwera. Nasłuchuje on za nowymi połączeniami, usuwa te nieaktywne
    # oraz odpowiada za przekazywanie informacji do wątków obsługi klientów.

    # ==================== Inicjalizacja ====================
    def __init__(self, main_thread, ip, port):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        # Inicjalizacja serwera na określonym przez użytkownika adresie ip i porcie
        self.server_ip = ip
        self.server_port = port
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.client_threads = []
        self.phone_msg_queue = []
        self.robot_msg_queue = []
        self.new_thread_event = threading.Event()
        self.is_running = False
        self.listening = False
        self.new_inactive_connection = False

    # ==================== Funkcja run ====================
    def run(self):
        # Funkcja nasłuchująca za nowymi połączeniami oraz usuwająca te nieaktywne

        self.server.bind((self.server_ip, self.server_port))  # Włączanie serwera na ip i porcie podanych w GUI
        self.new_thread_event.set()  # Ustawianie flagi eventu(konieczne do rozpoczęcia pierwszej pętli)
        # Wysyłanie informacji do GUI
        info = "<span style=\" font-size:8pt; font-weight:600; color:#0000ff;\" >"
        info += "Starting server on {}:{}".format(self.server_ip, str(self.server_port))
        info += "</span>"
        self.MAIN_THREAD.add_new_message(info)
        info = "Waiting for clients..."
        self.MAIN_THREAD.add_new_message(info)

        while self.is_running is True:
            # Wątek czeka, aż funkcja nasłuchująca zakończy działanie(dodany zostanie nowy klient), lub zakończy się
            # proces usuwania nieaktywnych połączeń
            self.new_thread_event.wait()
            self.new_thread_event.clear()  # Resetowanie flagi eventu
            if self.listening is False and (self.MAIN_THREAD.STATE.connection_with_phone is False
                                            or self.MAIN_THREAD.STATE.connection_with_robot is False):
                # Włączenie funkcji nasłuchiwania za nowymi klientami
                thread_listen = Thread(target=self.listen_for_connections)
                thread_listen.start()
            if self.new_inactive_connection is True:
                # Usuwanie nieaktywnych połączeń
                self.remove_inactive_connections()

    # ==================== Dodatkowe funkcje serwera ====================
    def listen_for_connections(self):
        # Funkcja nasłuchująca za nowymi połączeniami
        self.listening = True
        if self.is_running is True:
            self.server.listen(1)
            try:
                # Dodawanie wątku dla nowego klienta
                (connection, (ip, port)) = self.server.accept()
                connection.setblocking(False)
                new_thread = ClientThread(self.MAIN_THREAD, self, ip, port, connection)
                new_thread.start()
                self.client_threads.append(new_thread)
            except socket.error:
                self.is_running = False
            self.listening = False
            self.new_thread_event.set()

    def remove_inactive_connections(self):
        # Funkcja usuwająca nieaktywne połączenia
        new_thread_list = []
        for thread in self.client_threads:
            if thread.is_running is True:
                new_thread_list.append(thread)
            elif thread.isAlive() is True:
                thread.close_connection('error')
        self.client_threads = new_thread_list

    def update_message_queue(self, socket_type, msg):
        if socket_type == 'phone':
            self.phone_msg_queue.append(msg)
        elif socket_type == 'robot':
            self.robot_msg_queue.append(msg)

    def stop_server(self):
        # Zamykanie serwera
        self.is_running = False
        for thread in self.client_threads:
            thread.close_connection('server')
        self.server.close()
        info = "<span style=\" font-size:8pt; font-weight:600; color:#0000ff;\" >"
        info += "Stopping server {}:{}.".format(self.server_ip, self.server_port)
        info += "</span>"
        self.MAIN_THREAD.add_new_message(info)

    # ==================== Funkcje obsługi klientów ====================
    def set_robot_command(self, command):
        # Funkcja przekazająca wątkowi obsługi połączania z robotem dane do przesłania
        for thread in self.client_threads:
            if thread.socket_type == 'Robot socket':
                thread.messages_to_send.append(command)
                thread.new_message_to_send_event.set()

    def send_phone_request(self, request):
        # Funkcja przekazająca wątkowi obsługi połączania z telefonem dane do przesłania
        for thread in self.client_threads:
            if thread.socket_type == 'Mobile socket':
                thread.messages_to_send.append(request)
                thread.new_message_to_send_event.set()


# ==================== Funkcje obsługi klientów ====================
class ClientThread(Thread):
    # Wątek obsługujący połączenie z klientem, w tym odbieranie i wysyłanie danych

    # ==================== Inicjalizacja ====================
    def __init__(self, main_thread, server_thread, ip, port, connection):
        Thread.__init__(self)
        self.MAIN_THREAD = main_thread
        self.server_thread = server_thread
        self.ip = ip
        self.port = port
        self.connection = connection
        self.is_running = True
        self.getting_image = False
        self.enk_data_prev = np.zeros(3)
        # Zmienne obsługujące wysył wiadomości
        self.messages_to_send = []
        self.message_sender = Thread(target=self.message_sending_thread)
        self.new_message_to_send_event = threading.Event()
        self.message_sender.start()
        # "Typ" podłączonego klienta (Telefon albo robot)
        self.socket_type = ''
        # Zmienna zawierająca dane odbieranego obrazu
        self.image_data = b''

        # "Handshake" z klientem. Serwer wysyła wiadomość, odbiera wiadomość(typ Socketa klienta)
        # i znowu wysyła. Jeśli czynności te zostaną wykonane poprawnie to połączenie zostaje nawiązane.
        self.send_data('Welcome to server!')
        try:
            max_msg_num = 10
            for i in range(0, max_msg_num, 1):
                ready_to_read, _, _ = select.select((self.connection,), (), (), 10)
                if len(ready_to_read) == 0:
                    self.connection.close()
                    info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                    info += "Socket  {}:{} is invalid. Closing connection.".format(self.ip, self.port)
                    info += "</span>"
                    self.MAIN_THREAD.add_new_conn_message(info)
                    self.is_running = False
                else:
                    data_in = self.connection.recv(100)
                    msg = data_in.decode()
                    # Sprawdzamy czy nie znaleziono ciągu znaków 'Mobile' w otrzymanej wiadomości
                    if msg.find('Mobile') != -1:
                        if self.MAIN_THREAD.STATE.connection_with_phone is False:
                            self.MAIN_THREAD.change_connection_state('mobile', True)
                            self.socket_type = 'Mobile socket'
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#00ff00;\" >"
                            info += "Socket  {}:{} is {}".format(self.ip, self.port, self.socket_type)
                            info += "</span>"
                            self.MAIN_THREAD.add_new_conn_message(info)
                        else:
                            self.connection.close()
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                            info += "Socket {}:{} error - Mobile device is already connected on another socket!".format(
                                self.ip, self.port)
                            info += "</span>"
                            self.MAIN_THREAD.add_new_conn_message(info)
                            self.is_running = False
                        break
                    elif msg.find('Robot') != -1 or msg.find('IMU') != -1 or msg.find('Enk') != -1:
                        if self.MAIN_THREAD.STATE.connection_with_robot is False:
                            self.MAIN_THREAD.change_connection_state('robot', True)
                            self.socket_type = 'Robot socket'
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#00ff00;\" >"
                            info += "Socket {}:{} is {}".format(self.ip, self.port, self.socket_type)
                            info += "</span>"
                            self.MAIN_THREAD.add_new_conn_message(info)
                        else:
                            self.connection.close()
                            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                            info += "Socket {}:{} error - Robot is already connected on another socket!".format(
                                self.ip, self.port)
                            info += "</span>"
                            self.MAIN_THREAD.add_new_conn_message(info)
                            self.is_running = False
                        break
                    elif i == max_msg_num - 1:
                        self.connection.close()
                        info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
                        info += "Socket {}:{} is invalid. Closing connection.".format(self.ip, self.port)
                        info += "</span>"
                        self.MAIN_THREAD.add_new_conn_message(info)
                        self.is_running = False
        except (socket.error, ValueError):
            self.is_running = False
        self.send_data('Message received!')

    # ==================== Funkcja run ====================
    def run(self):
        # Funkcja obsługująca odbiór wiadomości z serwera
        while self.is_running is True:
            # Odbiór danych wysłanych przez klienta
            data_in = b''
            try:
                ready_to_read, _, _ = select.select((self.connection,), (), (), 60)
            except ValueError:
                self.close_connection('error')
            if len(ready_to_read) == 0:
                self.close_connection('error')
            else:
                while self.is_running is True:
                    try:
                        data_t = self.connection.recv(25000)
                        data_in += data_t
                        if len(data_t) < 25000:
                            break
                    except socket.error:
                        self.close_connection('error')
                        break

                if len(data_in) == 0:
                    # pusta wiadomość oznacza zerwanie połączenia, zamykamy wątek
                    self.close_connection('error')

                received_data = []
                len1 = 0
                len2 = 0
                for i in range(2, len(data_in)):
                    # Wykrywamy ciąg znaków $%$, który jest ustalonym przez nas separatorem wiadomości. Ciągi bajtów,
                    # pomiędzy którymi znajduje się ten separator są rozdzielane i zapisywane osobno w tablicy jako
                    # osobne wiadomości.
                    if data_in[i - 2] == 36 and data_in[i - 1] == 37 and data_in[i] == 36:
                        data_temp = data_in[len1: len2]
                        received_data.append(data_temp)
                        len1 = i + 1
                    else:
                        len2 = i - 1
                # Zapewnienie obsługi zapisu wiadomości, które nie używają separatora $%$
                received_data.append(data_in[len1: len(data_in)])

                for data in received_data:
                    # iterowanie po tablicy zawierającej rozdzielone wiadomości
                    try:
                        msg = data.decode()
                    except UnicodeDecodeError:
                        msg = ''
                    if self.getting_image is True and (msg[0:4] == '-@@-'):
                        # Ciąg znaków '-@@-'' sygnalizuje koniec wysyłania zdjęcia
                        if len(self.image_data) > 1:
                            self.send_data('Image received!')
                            self.getting_image = False
                            f = open('images/received.jpg', 'wb')
                            f.write(self.image_data)
                            f.close()
                            self.image_data = b''
                            img = cv2.imread('images/received.jpg')
                            if img is not None:
                                self.MAIN_THREAD.change_image_data(img)
                    elif self.getting_image is True:
                        # Jeśli odbieramy obraz to zapisujemy dane do zmiennej image_data.
                        # W przeciwnym razie traktujemy je jako zwykłą wiadomość.
                        self.image_data += data
                    else:
                        if msg[0:4] == '#@@#':
                            # Ciąg znaków '#@@#'' sygnalizuje rozpoczęcie wysyłania zdjęcia
                            self.getting_image = True
                        elif msg[0:4] == '-@@-':
                            # Ciąg znaków '-@@-'' sygnalizuje koniec wysyłania zdjęcia
                            self.getting_image = False
                        elif msg[0:5] == 'click':
                            # Ciąg znaków 'click' sygnalizuje przesył informacji o kliknięciu na ekran telefonu
                            try:
                                click_data = msg[5:].split('/')
                                # Sprawdzanie poprawności otrzymanych danych
                                click_data = np.asarray([float(click_data[0]), float(click_data[1])])
                                click_data = np.around(click_data, 1)
                                self.MAIN_THREAD.change_click_data(click_data)
                            except (ValueError, IndexError):
                                info = '{}: Error has occurred in received data!'.format(self.socket_type)
                                # self.MAIN_THREAD.add_new_message(info)
                        elif msg[0:4] == 'sens':
                            # Ciąg znaków 'sens' sygnalizuje przesył danych z sensorów z urzączenia mobilnego
                            self.send_data('Sensor data received!')
                            try:
                                sens_data = msg[4:].split('/')
                                # Sprawdzanie poprawności otrzymanych danych
                                sens_data = np.asarray([float(sens_data[0]), float(sens_data[1]), float(sens_data[2]),
                                                float(sens_data[3]), float(sens_data[4]), float(sens_data[5])])
                                sens_data = np.around(sens_data, 2)
                                self.MAIN_THREAD.change_sensor_data('phone', sens_data)
                            except (ValueError, IndexError):
                                info = '{}: Error has occurred in received data!'.format(self.socket_type)
                                # self.MAIN_THREAD.add_new_message(info)
                        elif msg[0:3] == 'IMU':
                            # Ciąg znaków 'IMU' sygnalizuje przesył danych z IMU z robota
                            try:
                                sens_data = np.asarray(msg[3:].split('/')[0:6], dtype=np.float32)
                                # czytaj pomiar z akcelerometru w osi X i rzutuj do m^2/s
                                sens_data[0:3] = sens_data[0:3] / 256.0
                                sens_data[3:6] = sens_data[3:6] / 14.4444  # czytaj pomiar z zyroskopu
                                self.MAIN_THREAD.change_sensor_data('robot', sens_data)
                            except (ValueError, IndexError):
                                info = '{}: Error has occurred in received data!'.format(self.socket_type)
                                self.MAIN_THREAD.add_new_message(info)
                        elif msg[0:3] == 'Enk':
                            # Ciąg znaków 'Enk' sygnalizuje przesył danych z enkoderów z robota
                            try:
                                enk_data_now = np.asarray(msg[3:].split('/')[0:3], dtype=np.float32)
                                if np.any(enk_data_now > 30000):
                                    self.enk_data_prev = np.zeros(3)
                                    enk_data_now = np.zeros(3)
                                    self.MAIN_THREAD.change_encoder_data(enk_data_now, True)
                                else:
                                    # enkodery są absolutne więc odejmujemy suma wartości do i-1 od wartosci i
                                    enk_data_now -= self.enk_data_prev
                                    self.enk_data_prev += enk_data_now
                                    self.MAIN_THREAD.change_encoder_data(enk_data_now, False)
                            except (ValueError, IndexError):
                                info = '{}: Error has occurred in received data!'.format(self.socket_type)
                            # self.MAIN_THREAD.add_new_message(info)
                        elif msg[0:13] == 'Disconnecting':
                            # Klient informuje serwer o tym, że będzie zamykał połączenie
                            self.close_connection('client')
                        elif msg[0:3] == 'req':
                            # Ciąg znaków 'req' sygnalizuje otrzymanie żądania z telefonu(np. żądanie kalibracji)
                            self.MAIN_THREAD.add_phone_request(msg[3:])
                        else:
                            if self.getting_image is False and msg[0:3] == 'msg':
                                # Ciąg znaków 'msg' sygnalizuje otrzymanie wiadomości do wyświetlenia
                                info = 'Received message from {}: {}'.format(self.socket_type, msg)
                                self.MAIN_THREAD.add_new_message(info)

    # ==================== Dodatkowe funkcje ====================
    def message_sending_thread(self):
        # Funkcja włączana jako osobny wątek, która odpowiada za wysył wiadomości do klienta
        while self.is_running is True:
            # oczekiwanie na informację o konieczności przesłania nowej wiadomości przez ustawienie flagi
            self.new_message_to_send_event.wait()
            self.new_message_to_send_event.clear()  # resetowanie flagi
            for msg in self.messages_to_send:
                self.send_data(msg)
            self.messages_to_send = []

    def send_data(self, msg_out):
        # Funkcja kodująca i wysyłająca wiadomość na klienta
        data_out = bytes(msg_out + '\n', 'utf-8')
        try:
            self.connection.sendall(data_out)
        except socket.error:
            self.close_connection('error')

    def get_connection_info(self):
        return str(self.ip + ':' + str(self.port))

    def close_connection(self, disconnect_source):
        # Metoda zatrzymuje połączenie z klientem i przekazuje informacje o tym,
        # z którym urządzeniem rozłączono się lub utracono łączność
        self.message_sender = None
        self.connection.close()
        self.is_running = False
        self.server_thread.new_inactive_connection = True
        self.server_thread.new_thread_event.set()
        if disconnect_source == 'error':
            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
            info += "Connection error in {} at {}:{}. Closing connection.".format(self.socket_type, self.ip, self.port)
            info += "</span>"
            self.MAIN_THREAD.add_new_conn_message(info)
        elif disconnect_source == 'client':
            info = "<span style=\" font-size:8pt; font-weight:600; color:#ff0000;\" >"
            info += "{} at {}:{} has disconnected from server.".format(self.socket_type, self.ip, self.port)
            info += "</span>"
            self.MAIN_THREAD.add_new_conn_message(info)
        # Szukamy ciągu znaków 'Mobile' w typie Socketa
        if self.socket_type == 'Mobile socket':
            self.MAIN_THREAD.change_connection_state('mobile', False)
        elif self.socket_type == 'Robot socket':
            self.MAIN_THREAD.change_connection_state('robot', False)



