import sys
import math
import cv2
import numpy as np
import threading
from threading import Thread
import TCP_server
import image_processing
import kalman_filter
import GUI
import robot_control
import time


default_img_path = 'images/android_temp.jpg'


# ==================== Głowny wątek programu ====================
class MainThread(Thread):
    # Wątek ten odpowiada za obsługę komunikacji i przekazywania danych między innymi wątkami działającymi w programie.

    # ==================== Inicjalizacja ====================
    def __init__(self):
        Thread.__init__(self)
        # zmienne wewnętrzne
        self.program_is_running = True
        self.STATE = State()
        self.EVENTS = Events()
        # inicjalizacja GUI
        self.GUI_window = GUI.Ui_MainWindow(self)
        self.GUI_window.show()
        # inicjalizacja wątków
        self.server_thread = TCP_server.ConnectionListenerThread(self, 'localhost', 5000)
        self.img_processing_thread = image_processing.ImgThread(self)
        self.robot_control_thread = robot_control.RobotControlThread(self)
        self.kalman_thread = kalman_filter.KalmanFilterThread(self)

    # ==================== Funkcja run ====================
    def run(self):
        # funkja odpowiada za przekazywanie nowych informacji(jeśli się takie pojawią) do innych wątków
        robot_status = 0
        while self.program_is_running is True:
            # Wątek oczekuje na informację o konieczności przesłania danych przez zmianę flagi new_information_event
            self.EVENTS.new_information_event.wait()
            # Wątek resetuje flagę new_information_event
            self.EVENTS.new_information_event.clear()
            # Event - nowa klatka obrazu
            if self.EVENTS.new_cam_frame is True:
                # Przeprowadzanie kalibracji robota
                if self.EVENTS.calibration_in_process is True:
                    if self.img_processing_thread.robot_calibration_steps_remaining > 0:
                        self.change_robot_control_variables([self.STATE.max_robot_velocity, 0, 0])
                    else:
                        self.robot_control_thread.stop = True
                        self.robot_control_thread.new_steering_event.set()
                        info = "<span style=\" font-size:8pt; font-weight:600; color:#ff00ff;\" >"
                        info += "Kalibracja robota zakończona!"
                        info += "</span>"
                        self.add_new_message(info)
                        self.robot_calibration_completed()
                self.img_processing_thread.image = self.STATE.image
                self.GUI_window.update_image(self.STATE.image)
                self.EVENTS.new_cam_frame = False
            # Event - nowe dane z sensorów z telefonu
            if self.EVENTS.new_phone_sensor is True:
                self.GUI_window.update_phone_sensor_text(self.STATE.phone_sensor)
                self.EVENTS.new_phone_sensor = False
            # Event - nowe dane z IMU z robota
            if self.EVENTS.new_robot_sensor is True:
                self.kalman_thread.IMU = self.STATE.robot_IMU
                self.GUI_window.update_robot_IMU_text(self.STATE.robot_IMU)
                self.EVENTS.new_robot_sensor = False
            # Event - nowe dane z enkoderów z robota
            if self.EVENTS.new_robot_enk is True:
                self.kalman_thread.enkoders = self.STATE.robot_enk
                self.GUI_window.update_encoders(self.STATE.robot_enk)
                self.EVENTS.new_robot_enk = False
            # Event - nowe klikniecie na ekran
            if self.EVENTS.new_click is True:
                if self.img_processing_thread.robot_calibration_complete is True \
                        and self.STATE.robot_control_mode == 'click':
                    info = "<span style=\" font-size:8pt; font-weight:600; color:#ff00ff;\" >"
                    info += "Rozpoczęto wyznaczanie trasy..."
                    info += "</span>"
                    self.add_new_message(info)
                    self.img_processing_thread.click = self.STATE.click
                    self.img_processing_thread.route_mapping_mode = True
                self.GUI_window.update_phone_click_text(self.STATE.click)
                self.EVENTS.new_click = False
            # Event - nowe dane do sterowania robotem
            if self.EVENTS.new_robot_control_variables is True:
                self.robot_control_thread.change_control_variables(self.STATE.robot_control_variables)
                self.EVENTS.new_robot_control_variables = False
            # Event - dajemy komendę na robota
            if self.EVENTS.curr_robot_command != '':
                self.server_thread.set_robot_command(self.EVENTS.curr_robot_command)
                self.EVENTS.curr_robot_command = ''
            # Event - zmiana pozycji wyznaczonej na obrazie
            if self.EVENTS.new_robot_image_position is True:
                self.kalman_thread.robot_image_position = self.STATE.robot_image_position
                self.EVENTS.new_robot_image_position = False
            # Event - zmiana pozycji wyznaczonej przez filtr Kalmana
            if self.EVENTS.new_robot_kalman_position is True:
                self.img_processing_thread.robot_kalman_position = self.STATE.robot_kalman_position
                self.EVENTS.new_robot_kalman_position = False
            # Event - wypisywanie wiadomości na GUI
            if len(self.EVENTS.gui_msg_array) > 0:
                for msg in self.EVENTS.gui_msg_array:
                    self.GUI_window.append_info_textbox(msg)
                self.EVENTS.gui_msg_array = []
            # Event - kolejka wiadomości wysyłanych na telefon nie jest pusta
            if len(self.EVENTS.phone_msg_queue) > 0:
                msg = self.EVENTS.phone_msg_queue[0]
                if msg != 'robot' and self.EVENTS.phone_msg_queue[0] != 'robot':
                    msg = self.EVENTS.phone_msg_queue.pop(0)
                    self.server_thread.send_phone_request(msg)
                elif 'robot' == self.EVENTS.phone_msg_queue[0] and robot_status % 500 == 0:
                    msg = self.EVENTS.phone_msg_queue.pop(0)
                    self.server_thread.send_phone_request(msg)
                    robot_status += 1
                    # print(robot_status)
                else:
                    self.EVENTS.phone_msg_queue.pop(0)
                    robot_status += 1

    # ==================== Dodatkowe funkcje ====================
    # Funkcje te są wywoływane przez wątki podrzędne w celu przekazania informacji wątkowi głównemu

    # ========== Funkcje serwera ==========
    def start_server(self, ip, port):
        self.server_thread = TCP_server.ConnectionListenerThread(self, ip, port)
        self.server_thread.is_running = True
        if self.server_thread.isAlive() is False:
            self.server_thread.start()

    def stop_server(self):
        self.server_thread.stop_server()

    def change_image_data(self, img):
        self.STATE.image = img
        self.EVENTS.new_cam_frame = True
        self.EVENTS.new_information_event.set()

    def change_click_data(self, click):
        new_click_time = time.time()
        if (new_click_time - self.EVENTS.last_click_time) > 0.5:
            self.EVENTS.last_click_time = new_click_time
            self.STATE.click = click
            self.EVENTS.new_click = True
            self.EVENTS.new_information_event.set()

    def change_encoder_data(self, enk_data_now, robot_reset_needed):
        if robot_reset_needed is False:
            # kąty pod którymi są silniki w stosunku do srodka robota
            fi = np.asarray([math.pi / 6, math.pi - math.pi / 6, math.pi + math.pi / 2])
            R_enc = 0.05
            time_ENK = 0.4
            # Tu sa m/s
            enk_data_now = (enk_data_now / (time_ENK * 8 * 78)) * 0.05 * math.pi

            # Model kinematyczny robota:
            v_x_loc = (2.0 / 3.0) * (
                    -math.sin(fi[0]) * enk_data_now[0] - math.sin(fi[0] + fi[1]) * enk_data_now[1] -
                    math.sin(fi[0] + fi[2]) * enk_data_now[2])
            v_y_loc = (2.0 / 3.0) * (
                    math.cos(fi[0]) * enk_data_now[0] + math.cos(fi[0] + fi[1]) * enk_data_now[1] +
                    math.cos(fi[0] + fi[2]) * enk_data_now[2])
            w = (2.0 / 3.0) * ((1.0 / (2.0 * R_enc)) * (enk_data_now[0] + enk_data_now[1] +
                                                        enk_data_now[2]))
            enk_data = [v_x_loc, v_y_loc, w]
        else:
            self.robot_control_thread.reset = True
            self.robot_control_thread.new_steering_event.set()
            enk_data = np.zeros(3)
        self.STATE.robot_enk = enk_data
        self.EVENTS.new_robot_enk = True
        self.EVENTS.new_information_event.set()

    def change_sensor_data(self, socket_type, sensor_data):
        if socket_type == 'phone':
            self.STATE.phone_sensor = sensor_data
            self.EVENTS.new_phone_sensor = True
            self.EVENTS.new_information_event.set()
        elif socket_type == 'robot':
            self.STATE.robot_IMU = sensor_data
            self.EVENTS.new_robot_sensor = True
            self.EVENTS.new_information_event.set()

    def add_phone_request(self, request):
        if request == 'calibration':
            info = "<span style=\" font-size:8pt; font-weight:600; color:#000000;\" >"
            info += "Otrzymano żądanie kalibracji z telefonu.\n"
            if self.STATE.robot_control_mode == 'click':
                self.set_robot_stop()
                self.img_processing_thread.calibration_request = True
                info += "Rozpoczęcie kalibracji koloru..."
            else:
                info += "Sterowanie robota ustawione jako manualne!\n"
                info += "Żądanie zostało zignorowane."
            info += "</span>"
            self.add_new_message(info)
        if request == 'stop':
            info = "<span style=\" font-size:8pt; font-weight:600; color:#000000;\" >"
            info += "Otrzymano żądanie zatrzymania robota z telefonu.\n"
            if self.STATE.robot_control_mode == 'click':
                self.add_new_message("Robot zatrzymany.")
                self.set_robot_stop()
            else:
                info += "Sterowanie robota ustawione jako manualne!\n"
                info += "Żądanie zostało zignorowane."
            info += "</span>"
            self.add_new_message(info)

    def add_phone_command(self, command):
        self.EVENTS.phone_msg_queue.append(command)
        self.EVENTS.new_information_event.set()

    def change_connection_state(self, socket_type, conn_val):
        if socket_type == 'mobile':
            self.STATE.connection_with_phone = conn_val
            if conn_val is True:
                self.GUI_window.phone_connection_info(conn_val)
                self.img_processing_thread = image_processing.ImgThread(self)
                self.img_processing_thread.is_running = True
                if self.img_processing_thread.isAlive() is False:
                    self.img_processing_thread.start()
            else:
                self.img_processing_thread.is_running = False
                self.GUI_window.phone_connection_info(conn_val)
                self.GUI_window.update_image(cv2.imread(default_img_path))
                self.GUI_window.update_processed_image(cv2.imread(default_img_path))
        elif socket_type == 'robot':
            self.STATE.connection_with_robot = conn_val
            if conn_val is True:
                self.GUI_window.robot_connection_info(conn_val)
                self.robot_control_thread = robot_control.RobotControlThread(self)
                self.robot_control_thread.is_running = True
                if self.robot_control_thread.isAlive() is False:
                    self.robot_control_thread.start()
            else:
                self.robot_control_thread.is_running = False
                self.GUI_window.robot_connection_info(conn_val)

    # ========== Funkcje kontroli robota ==========
    def set_robot_command(self, command):
        self.EVENTS.curr_robot_command = command
        self.EVENTS.new_information_event.set()

    def set_robot_stop(self):
        self.robot_control_thread.stop = True
        self.robot_control_thread.new_steering_event.set()

    # ========== Funkcje GUI ==========
    def add_new_message(self, message):
        self.EVENTS.gui_msg_array.append(message)
        self.EVENTS.new_information_event.set()

    def add_new_conn_message(self, message):
        if self.GUI_window.ui.conn_opt_checkbox.isChecked() is True:
            self.EVENTS.gui_msg_array.append(message)
            self.EVENTS.new_information_event.set()

    def set_robot_control_mode(self, mode):
        self.STATE.robot_control_mode = mode

    # ========== Funkcje image processing ==========
    def change_robot_img_position(self, position):
        self.STATE.robot_image_position = position
        self.EVENTS.new_robot_image_position = True
        self.EVENTS.new_information_event.set()

    def change_robot_control_variables(self, control_variables):
        if self.img_processing_thread.isAlive() and self.img_processing_thread.robot_found is True:
            self.STATE.robot_control_variables = control_variables
            self.EVENTS.new_robot_control_variables = True
            self.EVENTS.new_information_event.set()
        else:
            self.robot_control_thread.stop = True
            self.robot_control_thread.new_steering_event.set()

    def set_gui_frame2d(self, frame):
        self.GUI_window.update_processed_image(frame)

    def set_gui_robot_vel(self, velocity, rotation):
        self.GUI_window.update_robot_vel_text(velocity, rotation)

    def set_gui_robot_position(self, current, next):
        self.GUI_window.update_robot_position(current, next)

    def robot_calibration_completed(self):
        # Po udanej kalibracji następuje włączenie części programu odpowiedzialnych za wyznaczanie trasy
        self.EVENTS.calibration_in_process = False
        self.kalman_thread = kalman_filter.KalmanFilterThread(self)
        self.kalman_thread.is_running = True
        if self.kalman_thread.isAlive() is False:
            self.kalman_thread.start()

    # ========== Funkcje filtru Kalmana ==========
    def change_robot_kalman_position(self, position):
        self.STATE.robot_kalman_position = position
        self.EVENTS.new_robot_kalman_position = True
        self.EVENTS.new_information_event.set()


# ==================== Dodatkowe klasy ====================
class State:
    # Klasa zawierająca dane przekazywane pomiędzy wątkami(np. obraz z kamery telefonu)
    def __init__(self):
        self.image = cv2.imread(default_img_path)
        self.click = [-1, -1]
        self.phone_sensor = np.zeros(6)
        self.robot_IMU = np.zeros(6)
        self.robot_enk = np.zeros(3)
        self.robot_control_variables = np.zeros(3)
        self.robot_image_position = np.zeros(2)
        self.robot_kalman_position = np.zeros(2)
        self.connection_with_phone = False
        self.connection_with_robot = False
        self.robot_control_mode = 'click'  # manual - z klawiatury; click - kliknięcie na ekran
        self.max_robot_velocity = 30


class Events:
    # Klasa zawierająca informacje o nowych zdarzeniach, które nastąpiły w programie(np. przyszedł nowy obraz z kamery)
    def __init__(self):
        self.new_information_event = threading.Event()
        self.new_cam_frame = False
        self.new_phone_sensor = False
        self.new_click = False
        self.last_click_time = time.time()
        self.new_robot_sensor = False
        self.new_robot_enk = False
        self.new_robot_control_variables = False
        self.curr_robot_command = ''
        self.gui_msg_array = []
        self.phone_msg_queue = []
        self.robot_msg_queue = []
        self.robot_reset_ocurred = False
        self.calibration_in_process = False
        self.new_robot_image_position = 0
        self.new_robot_kalman_position = 0


# ==================== Rozpoczęcie działania programu ====================
app = GUI.QApplication(sys.argv)
main_app_thread = MainThread()
main_app_thread.start()
sys.exit(app.exec_())
