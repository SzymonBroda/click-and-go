# ------------------------------------------------------------------------------
# Funkcje uzytkowe
# ------------------------------------------------------------------------------
import cv2
import numpy as np


def find_objects(img, matrix, orange_lo, orange_hi, robot_lo, robot_hi, calibrator_area):
    """
    Funkcja otrzymuje obraz z telefonu, macierz przeształcenia, zakresy HSV oraz wymiary kalibratora.
    Funkcja zwraca biały obraz z czarnymi przeszkodami i robotem w postaci koła, współrzędne x i y środka robota oraz jego promień.
    """

    img = cv2.resize(img, (600, 400))  # resize żeby zmieściło się na ekranie, docelowo zbędne raczej
    blurred = cv2.blur(img, (5, 5))
    warped = cv2.warpPerspective(blurred, matrix, (600, 400))  # obraz w perspektywie

    # wszystkie wykrywania obiektów robimy na przekształconym na rzut z góry obrazie
    orange_threshed = cv2.inRange(warped, orange_lo, orange_hi)  # obrazy wycięte na podstawie zakresów

    # poprawienie jakości progowania
    # kernel = np.ones((8, 8), np.uint8)
    # orange_threshed = cv2.morphologyEx(orange_threshed, cv2.MORPH_CLOSE, kernel)
    # orange_threshed = cv2.erode(orange_threshed, kernel, 2)
    # orange_threshed = cv2.dilate(orange_threshed, kernel, 8)

    robot_threshed = cv2.inRange(warped, robot_lo, robot_hi)

    # kernel = np.ones((3, 3), np.uint8)
    # robot_threshed = cv2.morphologyEx(robot_threshed, cv2.MORPH_CLOSE, kernel)
    # robot_threshed = cv2.erode(robot_threshed, kernel, 3)
    # robot_threshed = cv2.dilate(robot_threshed, kernel, 3)

    orange_contours, _ = cv2.findContours(orange_threshed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)  # kontury
    robot_contours, _ = cv2.findContours(robot_threshed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    general_mask = np.ones(warped.shape[:2], dtype="uint8") * 255  # na tej masce będziemy rysować

    # rysowanie pudełek
    min_area = 0.4 * calibrator_area
    max_area = 1.3 * calibrator_area
    for cnt in orange_contours:
        area = cv2.contourArea(cnt)
        approx = cv2.approxPolyDP(cnt, 0.05 * cv2.arcLength(cnt, True), True)
        # ograniczenie rozmiaru aby odfioltrować pudełka od szumu
        if min_area < area < max_area:
            # rysujemy przybliżony przez funkcję approx kontur
            cv2.drawContours(general_mask, [cv2.convexHull(approx)], -1, 0, -1)
            # print("Pudelko o rozmiarze {}".format(area))

    # rysowanie robota
    robot_found = False
    min_area = 0.2 * calibrator_area
    max_area = 0.9 * calibrator_area
    biggest_contour = 0
    biggest_index = 0
    for i, cnt in enumerate(robot_contours):
        area = cv2.contourArea(cnt)
        # ograniczenie rozmiaru aby odfiltrowac robota od szumu
        if min_area < area < max_area and area > biggest_contour:
            max_area = area
            biggest_index = i
            biggest_contour = area
            robot_found = True
    if robot_found:
        radius = int(np.sqrt(1.2*biggest_contour / np.pi))  # aproksymacja konturu kolem
        moments = cv2.moments(robot_contours[biggest_index])  # liczenie momentow konturu (dane na temat ksztaltu)
        center_x = int(moments["m10"] / moments["m00"])  # z momentow mamy srodek konturu
        center_y = int(moments["m01"] / moments["m00"])
        # rysujemy przyblizony kolem kontur
        general_mask = cv2.circle(general_mask, (center_x, center_y), radius, color=2, thickness=-1)
        return cv2.cvtColor(general_mask, cv2.COLOR_GRAY2BGR), (center_x, center_y), radius, True
    else:
        # print("Nie wykryto robota.")
        # zahardkodowane parametry - rozwiazanie raczej tymczasowe
        return cv2.cvtColor(general_mask, cv2.COLOR_GRAY2BGR), (10, 10), 5, False


def transform_click(x, y, img, matrix):
    """
    Funkcja otrzymuje znormalizowane współrzędne kliknięcia na ekranie, obraz z telefonu oraz macierz perspektywy..
    Funkcja zwraca zmapowane współrzędne kliknięcia.
    """

    img = cv2.resize(img, (600, 400))
    new_x = -1
    new_y = -1

    # Przesyłamy współrzędne znormalizowane do rozmiaru ekranu telefonu (x / rozdzielczość w x).
    # Dla łatwiejszego przesyłu mnożymy je przez 1000, więc poniżej dzielę je przez 1000.
    x = int(img.shape[1] * x / 1000)  # Mnożymy przez wymiar obrazu i mamy realne współrzędne kliknięcia na obraz
    y = int(img.shape[0] * y / 1000)
    print("\nKlikniecie przed mapowaniem:")
    print(x, y)

    # Pusta maska, na której narysujemy miejsce kliknięcia
    unwarped_mask = np.ones(img.shape[:2], dtype="uint8") * 255
    unwarped_mask = cv2.circle(unwarped_mask, (x, y), 8, color=2, thickness=-1)

    # Zmapowana maska (odwrócona żeby znaleźć kontury)
    warped_mask = cv2.warpPerspective(unwarped_mask, matrix, (600, 400))
    warped_mask = cv2.bitwise_not(warped_mask)
    contours, _ = cv2.findContours(warped_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Znajdujemy najmniejszy kontur (narysowane miejsce kliknięcia)
    smallest = 1000
    smallest_index = 0
    for i, cnt in enumerate(contours):
        area = cv2.contourArea(cnt)
        if area < smallest:
            smallest = area
            smallest_index = i

    # Liczymy środek konturu korzystając z momentów
    try:
        moments = cv2.moments(contours[smallest_index])  # liczenie momentow konturu (dane na temat ksztaltu)
        if moments["m00"] != 0 and smallest < 1000:  # unikanie dzielenia przez zero
            new_x = int(moments["m10"] / moments["m00"])  # z momentów mamy środek konturu
            new_y = int(moments["m01"] / moments["m00"])
    except IndexError:
        print("Index error przy wyznaczaniu zmapowanego klikniecia")     

    # Zapis masek dla ułatwienia testowania
    cv2.imwrite("images/unwarped_click.jpg", unwarped_mask)
    cv2.imwrite("images/warped_click.jpg", warped_mask)

    print("Klikniecie zmapowane:")
    print(new_x, new_y)
    return [new_x, new_y]


def calibrate(img):
    """
    Funkcja otrzymuje obraz z telefonu.
    Funkcja zwraca czy kalibracja się powiodła, RGB korekcyjne, macierz przekształcenia perspektywy oraz pole
    powierzchni kalibratora.
    """

    white_lo = np.array([0, 0, 150], np.uint8)  # kolor kartki
    white_hi = np.array([180, 50, 255], np.uint8)
    img = cv2.resize(img, (600, 400))  # resize żeby zmieściło się na ekranie, docelowo zbędne raczej
    blurred = cv2.blur(img, (6, 6))
    hsv_img = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)  # obrazek w odpowiednim formacie kolorow, lekko rozmyty

    white_threshed = cv2.inRange(hsv_img, white_lo, white_hi)  # obrazek po progowaniu
    # poprawa jakości progowanego obrazu
    kernel = np.ones((5, 5), np.uint8)
    white_threshed = cv2.erode(white_threshed, kernel, 2)
    white_threshed = cv2.dilate(white_threshed, kernel, 1)
    white_contours, _ = cv2.findContours(white_threshed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)  # znalezienie konturów
    white_mask = np.ones(img.shape[:2], dtype="uint8") * 255  # pusta maska

    # rysowanie kalibratora jako największego białego prostokąta
    largest = 0
    largest_index = 0
    for i, cnt in enumerate(white_contours):
        area = cv2.contourArea(cnt)
        approx = cv2.approxPolyDP(cnt, 0.08 * cv2.arcLength(white_contours[i], True), True)
        # odfiltrowanie małych i bardzo dużych kształtów
        if 2000 < area < 40000 and 4 == len(approx) and area > largest:
            moments = cv2.moments(cnt)  # liczenie momentow konturu (dane na temat ksztaltu)
            if moments["m00"] == 0:  # unikanie dzielenia przez zero
                continue
            center_x = int(moments["m10"] / moments["m00"])  # z momentow mamy srodek konturu
            center_y = int(moments["m01"] / moments["m00"])
            # wiemy mniej więcej gdzie znajduje się kalibrator
            if center_x > 300 and center_y > 200:
                largest = area
                largest_index = i
                # print("Nowy najwiekszy kontur o wymiarze {}".format(area))

    # rysowanie przybliżonego kształtu aby ułatwić skryptowi get_points wykrycie go
    approx = cv2.approxPolyDP(white_contours[largest_index], 0.04 * cv2.arcLength(white_contours[largest_index], True), True)
    cv2.drawContours(white_mask, [approx], -1, 0, -1)

    # dostajemy współrzędne rogów kalibratora, zwraca również calibrated = False gdy się nie powiedzie
    tl, tr, br, bl, calibrated = get_points(white_mask)

    # fragment odpowiedzialny za wyznaczenie skali, tu trzeba jeszcze to podrasować
    top = np.sqrt((tr[0] - tl[0]) ** 2 + (tr[1] - tl[1]) ** 2)
    left = np.sqrt((tl[0] - bl[0]) ** 2 + (tl[1] - bl[1]) ** 2)
    shorter = min(top, left)
    # współczynnik przeskalowania perspektywy
    k = 0.038 * shorter * 1.4  # wyznaczone na podstawie jednego dobrego przypadku, powinno dzialac w ogolnosci
    # 64 piksele to 21 cm, 305 pikseli to 1 metr
    # print("1 metr = 305 px")

    points = np.float32([tl, tr, br, bl])  # przekształcenie punktów na odpowiednią formę

    # pozycja kalibratora na obrazie wynikowym (idealnie prawy dolny róg, rozmiar jest kartki A4 21x30)
    dst = np.array([[600 - 30 * k, 400 - 21 * k], [600, 400 - 21 * k], [600, 400], [600 - 30 * k, 400]],
                   dtype="float32")
    matrix = cv2.getPerspectiveTransform(points, dst)  # znajdujemy macierz perspektywy

    # kalibracja kolorów
    white_mask = cv2.bitwise_not(white_mask)
    calibrator = cv2.bitwise_or(img, img, mask=white_mask)  # znajdujemy czarny obraz z 'kolorowym' kalibratorem
    # znajdujemy elementy, które nie są czarne (czyli są kalibratorem)
    calibrator = [list(sub) for val in calibrator for sub in val if sub[0] != 0 and sub[1] != 0 and sub[2] != 0]
    calibrator = np.array(calibrator)
    average = calibrator.mean(axis=0)  # liczymy średnie RGB kalibratora
    # kartka powinna być (255, 255, 255) a jest (x, y, z), więc aby zrobić korektę trzeba dodać 255-średnia
    average = 255 - average

    # zapis obrazów ułatwiających debugowanie w przypadku nie wykrycia kalibratora / dziwnej perspektywy
    cv2.imwrite("images/white_threshed.jpg", white_threshed)
    cv2.imwrite("images/white_mask.jpg", white_mask)

    return calibrated, average[0], average[1], average[2], matrix, largest


def rgb_to_hsv(r, g, b):
    """
    Funkcja otrzymuje wartości RGB.
    Funkcja zwraca wartości HSV.
    """
    # Ta funkcja na ten moment nie jest używana, można to zrobić lepiej w openCV

    r = r / 255
    g = g / 255
    b = b / 255
    cmax = max(r, g, b)
    cmin = min(r, g, b)
    diff = cmax - cmin

    v = cmax * 100

    if cmax == 0:
        s = 0
    else:
        s = 100 * diff / cmax

    if cmax == 0:
        h = 0
    elif cmax == r:
        h = 60 * (((g - b) / diff) + 360) % 360
    elif cmax == g:
        h = 60 * ((b - r) / diff + 120) % 360
    else:
        h = 60 * ((r - g) / diff + 240) % 360

    return h / 2, s * 255 / 100, v * 255 / 100


def order_points(pts):
    """
    Funkcja ma na celu uporządkować punkty w kolejności:
    lewy górny(tl),
    prawy górny(tr),
    prawy dolny(br),
    lewy dolny(bl)
    """
    # inicjalizacja tablicy w której znajdą sie wspołrzędne punktów w ustalonej kolejności
    rect = np.zeros((4, 2), dtype="float32")

    # Definiowanie wierzchołków korzystając z sumy (xn + yn)
    # Lewy górny wierzchołek będzie miał najmniejszą sumę, a prawy dolny wierzchołek największą
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]

    # Definiowanie wierzchołków korzystając z roznicy (yn - xn)
    # Prawy górny wierzchołek ma najmniejszą różnicę, a lewy dolny największą
    d = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(d)]
    rect[3] = pts[np.argmax(d)]

    # Zwracam uporządkowane wg wspomnianej konwencji punkty
    return rect


def get_rect_area(p1, p2, p3, p4):
    """
    p1 - tl, p2 - tr,
    p4 - bl, p3 - br
    Funkcja zwraca pole powierzchni prostokąta na podstawie otrzymanych punktów
    """
    # Pole prostokąta P = a*b
    # Dlugosc odcinka |AB| = √(x2-x1)^2 + (y2-y1)^2

    # Wyznaczenie maksymalnej szerokości.
    # prawy gorny - lewy gorny lub prawy dolny - lewy dolny
    width_a = np.sqrt(((p2[0] - p1[0]) ** 2) + ((p2[1] - p2[1]) ** 2))
    width_b = np.sqrt(((p3[0] - p4[0]) ** 2) + ((p3[1] - p4[1]) ** 2))
    width_array = np.array([width_a, width_b])
    width_max = np.max(width_array)

    # Wyznaczanie maksymalnej wysokosci
    # lewy dolny - lewy gorny lub prawy dolny - prawy gorny
    height_a = np.sqrt(((p4[0] - p1[0]) ** 2) + ((p4[1] - p1[1]) ** 2))
    height_b = np.sqrt(((p3[0] - p2[0]) ** 2) + ((p3[1] - p2[1]) ** 2))
    height_array = np.array([height_a, height_b])
    height_max = np.max(height_array)

    # Wyznaczenie pola powierzchni znalezionego prostokata
    rect_area = width_max * height_max

    return rect_area


def get_points(thresh):
    """
    Funkcja otrzymuje binarny obraz na którym znajduje się prostokąt.
    Funkcja zwraca współrzędne prostokąta.
    """
    found = False
    rect_area_array = np.array([])  # Tablica przechowująca pole powierzchni każdego znalezionego prostokata
    points_array = np.array([])  # Tablica przechowująca wszystkie współrzędne punktów
    dim = thresh.shape
    h, w = dim[0], dim[1]  # Wymiary podanego binarnego obrazu
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        approx = cv2.approxPolyDP(cnt, 0.009 * cv2.arcLength(cnt, True), True)

        # jeżeli kontur kwadratu/prostokąta (znaleziono 4 punkty)
        if len(approx) == 4:
            # warunek na pominiecie bboxa obejmującego całe zdjęcie
            if approx[0, 0, 0] == 0:
                # print(approx)
                continue
            else:
                coordinates = np.array([approx[0, 0], approx[1, 0], approx[2, 0], approx[3, 0]])

                # Wywołanie funkcji w celu uporządkowania współrzędnych
                rect = order_points(coordinates)
                (tl, tr, br, bl) = rect

                # Operacje do wydobycia współrzędnych największego prostokąta
                rect_area = get_rect_area(tl, tr, br, bl)
                rect_area_array = np.append(rect_area_array, rect_area)
                points_array = np.append(points_array, [tl, tr, br, bl])

    try:
        # Sprawdzenie ktory prostokat byl najwiekszy i przypisanie na tej podstawie współrzędnych
        index_max_area = np.argmax(rect_area_array)
        t_l = (int(points_array[8 * index_max_area]), int(points_array[8 * index_max_area + 1]))
        t_r = (int(points_array[8 * index_max_area + 2]), int(points_array[8 * index_max_area + 3]))
        b_r = (int(points_array[8 * index_max_area + 4]), int(points_array[8 * index_max_area + 5]))
        b_l = (int(points_array[8 * index_max_area + 6]), int(points_array[8 * index_max_area + 7]))
        found = True
    except ValueError:
        # W przypadku gdy na zdjęciu nie zostanie znaleziony prostokąt jako współrzędne podane zostaną wymiary zdjęcia
        print("Nie znaleziono, podaje wymiary zdjecia.")
        t_l = (0, 0)
        t_r = (0, h - 1)
        b_r = (w - 1, h - 1)
        b_l = (w - 1, 0)

    return t_l, t_r, b_r, b_l, found
