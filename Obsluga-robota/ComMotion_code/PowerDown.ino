void PowerDown()
{
  //=============================== Funkcja obslugujaca niski poziom baterii ===============================
  analogWrite(pwmapin, 0);              // Wylaczamy silnik A
  analogWrite(pwmbpin, 0);              // Wylaczamy silnik B
  eflag = eflag|B00100000;              // Bit 6 flagi bledow wskazuje wylaczenie z powodu niskiego poziomu baterii

  Wire.beginTransmission(address+1);    // Przejmujemy kontrole nad magistrala I2C i adresujemy MCU 2
  Wire.write(3);                        // Inicjalizujemy 3 komende - dane sterujaca silnikami
  for(byte i = 0; i < 8; i++)
  {
    Wire.write(0);                      // Zerujemy wszystkie wartosci sterowania silnikami
  }
  Wire.endTransmission();
}
