//=============================== Piny cyfrowe ===============================
#define encapin     2     // D02 PD2 (INT0) - pin sensora  enkodera  silnika A
#define encbpin     3     // D03 PD3 (INT1) - pin sensora  enkodera  silnika B
#define dirapin     8     // D08 PB0 (ICP1) - pin kontroli kierunku  silnika A
#define pwmapin     9     // D09 PB1 (OC1A) - pin kontroli predkosci silnika A
#define pwmbpin    10     // D10 PB2 (OC1B) - pin kontroli predkosci silnika B
#define dirbpin    11     // D11 PB3 (OC1C) - pin kontroli kierunku  silnika B
#define IDpin      16     // D16 PC2 (ADC2) - pin identyfikacji MCU   stan niski = MCU 1    stan wysoki = MCU 2

//=============================== Piny analogowe ===============================
#define curapin     0     //  A0 PC0 (ADC0) - pin sensora  natezenia silnika A
#define curbpin     1     //  A1 PC1 (ADC1) - pin sensora  natezenia silnika B
#define voltpin     7     //  A7     (ADC7) - napiecie na MCU 1
#define senspin     7     //  A7     (ADC7) - sensor na MCU 2
#define irpin      17
