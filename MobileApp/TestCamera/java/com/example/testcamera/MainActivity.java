package com.example.testcamera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.view.MotionEvent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;


public class MainActivity extends AppCompatActivity  implements SensorEventListener{
    private Camera mCamera;
    public CameraView mPreview;
    public String STRING_PORT;
    public String SERVER_IP = "none";
    public int SERVER_PORT = 5000;
    public float[] sensor_values;
    public float[] click_values;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor gyroscope;
    private float height;
    private float width;
    private Boolean wants_to_connect = false;
    TcpClientThread tcp_client = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        onWindowFocusChanged(true);
        sensor_values = new float[6];
        click_values = new float[2];
        click_values[0] = -1;
        click_values[1] = -1;

        setContentView(R.layout.activity_main);
        ImageButton btn = (ImageButton)findViewById(R.id.imageButton);
        btn.setBackgroundColor(Color.RED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (checkPermission()) {
            mCamera = getCameraInstance();
            mPreview = new CameraView(this, mCamera);
            FrameLayout preview = findViewById(R.id.camera_preview);
            preview.addView(mPreview);
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            try {
                accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            }
            catch (Exception e) {
                Log.e("Accelerometer: ", "Null pointer: " + e);
            }
            try {
                gyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            }
            catch (Exception e) {
                Log.e("Gyroscope: ", "Null pointer: " + e);
            }
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
            sensorManager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        }

        else
            requestPermission();
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                200);
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public void connect(View view) {
        Button connectbtn = (Button)findViewById(R.id.connectbtn);

        // PODLACZENIE DO SERWERA WEDLUG PODANEGO IP I PORTU
        if (wants_to_connect) {
            connectbtn.setText(R.string.connect);
            ImageButton btn = (ImageButton)findViewById(R.id.imageButton);
            btn.setBackgroundColor(Color.RED);
            Toast toast = Toast.makeText(getApplicationContext(), "Closing connection", Toast.LENGTH_SHORT);
            toast.setMargin(50, 50);
            toast.show();
            wants_to_connect = false;
            disconnect();
        }
        else {
            Toast toast = Toast.makeText(getApplicationContext(), SERVER_IP + ":" + STRING_PORT, Toast.LENGTH_SHORT);
            toast.setMargin(50, 50);
            toast.show();
            wants_to_connect = true;
            restart_connection();
        }
    }


    public void restart_connection() {
        //  Metoda, która zamyka bieżący wątek Socketa(jeśli jest taki otwarty) i otwiera nowy.
        //  Używana zarówno do początkowego przyłączenia do serwera, jak i automatycznej regenracji połączenia.
        if (wants_to_connect) {
            if (tcp_client != null) {
                tcp_client.close_connection();
                tcp_client = null;
            }
            tcp_client = new TcpClientThread(this, SERVER_IP, SERVER_PORT);
            Thread tcp_thread = new Thread(tcp_client);
            tcp_thread.start();
        }
        else {
            disconnect();
        }
    }


    public void disconnect() {
        //  Metoda zamyka bieżące połączenie z serwerem i zatrzymuje wątek Socketa.
        if (tcp_client != null) {
            tcp_client.disconnect_message();
            tcp_client.close_connection();
            tcp_client = null;
        }
    }


    public void calibration_request (View view){
        //  Metoda ta, po naciśnięciu przycisku, dodaje żądanie kalibracji do kolejki żądań Socketa
        if (tcp_client != null) {
            String data_str = "calibration";
            tcp_client.add_request(data_str);
        }
        else
            openDialog(1);
    }


    public boolean onTouchEvent(MotionEvent event) {
        // ============= Funkcja odczytująca parametry x i y z klikniętego miejsca i wysyłająca je na serwer================
        int pointerId = event.getPointerId(0);
        int pointerIndex = event.findPointerIndex(pointerId);
        // Get the pointer's current position
        float x = event.getX(pointerIndex);
        float y = event.getY(pointerIndex);

        // Przeskalowanie współrzędnych z ekranu - podzielenie przez wymiar i mnożenie razy 1000 (serwer przystosowany do odbioru inta)
        click_values[0] = 1000 * x/width;
        click_values[1] = 1000 * y/height;

        return false;
    }


    public void onSensorChanged(SensorEvent event) {
        // === Funkcja pobierająca dane z sensora i wyświetląjąca je na ekranie (tymczasowo)
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            sensor_values[0] = x;
            sensor_values[1] = y;
            sensor_values[2] = z;
        }
        if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            sensor_values[3] = x;
            sensor_values[4] = y;
            sensor_values[5] = z;
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    public void open_settings(View view) {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.putExtra ( "ip", SERVER_IP);
        intent.putExtra ( "port", STRING_PORT);

        startActivity(intent);
    }


    // POBRANIE DANYCH PODANYCH W USTAWIENIACH
    @Override
    protected void onResume() {
        super.onResume();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (SERVER_IP.equals("none")) {
            Bundle bb;
            bb = getIntent().getExtras();
            assert bb != null;
            if (!bb.getString("port").isEmpty()) {
                STRING_PORT = bb.getString("port");
                SERVER_PORT = Integer.parseInt(STRING_PORT);
            }
            if (!bb.getString("ip").isEmpty())
                SERVER_IP = bb.getString("ip");
        }

        else{
            SharedPreferences settings = getSharedPreferences("CaG", Context.MODE_PRIVATE);
            SERVER_IP = settings.getString("ip_address", "");
            STRING_PORT = settings.getString("port_number", "");
        }

        mCamera = null;
        mCamera = getCameraInstance();
        mPreview = new CameraView(this, mCamera);
        FrameLayout preview = findViewById(R.id.camera_preview);
        preview.removeAllViews();
        preview.addView(mPreview);
    }


    public static Camera getCameraInstance()
    //  Metoda, która otwiera i zwraca instancję kamery albo wyświetla błąd, jeśli nie może jej otworzyć
    {
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch(Exception e) {
            Log.e("Camera", "Camera cant be opened " + e);
        }
        return c;
    }


    public void openDialog(int w){
        CreateDialog dialog = new CreateDialog().which_one(w);
        dialog.show(getSupportFragmentManager(),"");
    }

    public void stop_command(View view) {
        tcp_client.add_request("stop");
        Toast toast = Toast.makeText(getApplicationContext(), "Closing connection", Toast.LENGTH_SHORT);
        toast.setMargin(50, 50);
        toast.show();
    }
}
